﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class ArtistHotKey
{
    [MenuItem("ArtistTool/GameObject/Toggle Active State _a", false, 0)]
    public static void CaptureAllSelected()
    {
        GameObject[] SelectedObject = Selection.gameObjects;
        if (SelectedObject.Length > 0)
        {
            Undo.RecordObjects(SelectedObject, "QAQ");
            bool aIsActive = !SelectedObject[0].activeInHierarchy;
            for (int Index = 0; Index < SelectedObject.Length; Index++)
            {
                SelectedObject[Index].SetActive(aIsActive);
                Debug.LogError(SelectedObject[Index] .name+ " "+aIsActive);
            }
        }
        else
        {
            Debug.LogError("你沒有選任何物件QAQ");
        }
    }
}
