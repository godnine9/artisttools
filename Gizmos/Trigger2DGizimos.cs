﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Trigger2DGizimos : MonoBehaviour
{
    public bool IsActiveable          = true;
    public Color OutLineColor         = new Color(255, 255, 255, 0.3f);
    public Color ActiveColor          = new Color(0  , 255, 0  , 0.15f);
    public Color SelectedOutlineColor = new Color(255, 150, 0  , 0.3f);
    public Color InActiveColor        = new Color(255,   0, 0  , 0.15f);

#if UNITY_EDITOR
    void OnDrawGizmos()
    {
        if (IsActiveable)
        {
            DrawColliders(this, ActiveColor, OutLineColor);
        }
        {
            DrawColliders(this, InActiveColor, OutLineColor);
        }
    }

    void OnDrawGizmosSelected()
    {
        DrawColliders(this, SelectedOutlineColor, OutLineColor , true);
    }
#endif

    public static void DrawColliders(MonoBehaviour mb, Color iBaseColor, bool iIsSelected = false)
    {
#if UNITY_EDITOR
        DrawColliders(mb, new Color(iBaseColor.r, iBaseColor.g, iBaseColor.b, 0.25f), new Color(iBaseColor.r, iBaseColor.g, iBaseColor.b, 1.0f), iIsSelected);
#endif
    }

    public static void DrawColliders(MonoBehaviour mb, Color meshColor, Color outlineColor, bool bSelected = false)
    {
#if UNITY_EDITOR
        if (bSelected)
        {
            Vector3 DisPlayPosition = mb.transform.position + mb.transform.up * 5;
            DebugDrawText.DrawText(DisPlayPosition, mb.name, new Color(1, 1, 1, 1), new Color(0, 0, 0, 0.7f));
        }
        else
        {
            outlineColor.a *= 0.5f;
        }

        Collider2D[] colliders = (mb.GetComponent<Rigidbody>()) ? mb.GetComponentsInChildren<Collider2D>() : mb.GetComponents<Collider2D>();

        foreach (Collider2D aCollider in colliders)
        {
            Gizmos.matrix = aCollider.transform.localToWorldMatrix;
            Gizmos.color = meshColor;

            if (aCollider.GetType() == typeof(BoxCollider2D))
            {
                BoxCollider2D boxCollider = (BoxCollider2D)aCollider;
                Gizmos.DrawCube(boxCollider.offset, boxCollider.size);
                Gizmos.color = outlineColor;
                Gizmos.DrawWireCube(boxCollider.offset, boxCollider.size);
            }
            else if (aCollider.GetType() == typeof(CircleCollider2D))
            {
                CircleCollider2D sphereCollider = (CircleCollider2D)aCollider;

                Gizmos.DrawWireSphere(sphereCollider.offset, sphereCollider.radius);
                Gizmos.color = outlineColor;
                Gizmos.DrawWireSphere(sphereCollider.offset, sphereCollider.radius);
            }
            //else if (aCollider.GetType() == typeof(PolygonCollider2D))
            //{
            //    PolygonCollider2D meshCollider = (PolygonCollider2D)aCollider;
            //    Gizmos.DrawMesh(meshCollider.sharedMesh);
            //    Gizmos.color = outlineColor;
            //    Gizmos.DrawWireMesh(meshCollider.sharedMesh);
            //}
        }
#endif
    }
}
