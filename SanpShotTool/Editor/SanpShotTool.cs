﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System;
using System.IO;

public class SanpShotTool
{
    [MenuItem("ArtistTool/Capture/Capture Current Screen _1", false, 0)]
    public static void CaptureScreen()
    {
        string folderPath = Directory.GetCurrentDirectory() + "/Screenshots/";
        if (!Directory.Exists(folderPath))
        {
            Directory.CreateDirectory(folderPath);
        }
        string Date = DateTime.Now.ToString("mm-ss");
        string screenshotName = string.Format("{0}{1}{2}", "Screenshot_", Date, ".png");
        ScreenCapture.CaptureScreenshot(Path.Combine(folderPath, screenshotName));
        Debug.LogError(folderPath + screenshotName);
    }

    [MenuItem("ArtistTool/Capture/Capture Selected _2", false, 0)]
    public static void CaptureSelected()
    {
        string folderPath = Directory.GetCurrentDirectory() + "/Screenshots/";
        if (!Directory.Exists(folderPath))
        {
            Directory.CreateDirectory(folderPath);
        }
        GameObject[] SelectedObject = Selection.gameObjects;
        if (SelectedObject.Length > 0)
        {
            Undo.RecordObjects(SelectedObject, "QAQ");
            SelectedObject[0].SetActive(true);
            string Date = DateTime.Now.ToString("mm-ss");
            string screenshotName = string.Format("{0}{1}{2}", SelectedObject[0].name, Date, ".png");
            ScreenCapture.CaptureScreenshot(Path.Combine(folderPath, screenshotName));
            Debug.LogError(folderPath + screenshotName);
        }
        else
        {
            Debug.LogError("你沒有選任何物件QAQ");
        }
    }
}
