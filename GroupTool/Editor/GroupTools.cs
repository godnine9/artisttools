﻿using UnityEditor;
using UnityEngine;

public static class GroupTools
{

    [MenuItem("ArtistTool/GroupTools/Group Selected %_g", false, 0)]
    static void GroupSelected()
    {
        GameObject[] SelectedObject = Selection.gameObjects;
        if (SelectedObject.Length > 0)
        {
            Undo.RecordObjects(SelectedObject,"QAQ");
            Transform CurrentParent = SelectedObject[SelectedObject.Length - 1].transform.parent;
            for (int idx = 0; idx < SelectedObject.Length; idx++)
            {
                SelectedObject[idx].transform.parent = SelectedObject[idx].transform.root;
            }
            for (int idx = 1; idx < SelectedObject.Length; idx++)
            {
                SelectedObject[idx].transform.parent = SelectedObject[0].transform;
            }
            SelectedObject[0].transform.parent = CurrentParent;
            CurrentParent.gameObject.SetActive(false);
            CurrentParent.gameObject.SetActive(true);
            RefreshHierarchy(SelectedObject[0]);
        }
        else
        {
            Debug.LogError("你沒有選任何物件QAQ");
        }
    }

    static void RefreshHierarchy(GameObject Parent)
    {
        Parent.gameObject.SetActive(false);
        Parent.gameObject.SetActive(true);
    }

    [MenuItem("ArtistTool/GroupTools/Group With Template %#_g", false, 0)]
    static void GroupWithTemplate()
    {
        GameObject[] SelectedObject = Selection.gameObjects;
        if (SelectedObject.Length > 0)
        {
            GameObject Template = new GameObject();
            Template.transform.parent = SelectedObject[0].transform.parent;
            Template.name = "Group";
            Undo.RecordObjects(SelectedObject, "QAQ");
            for (int idx = 0; idx < SelectedObject.Length; idx++)
            {
                SelectedObject[idx].transform.parent = Template.transform;
            }
        }
        else
        {
            Debug.LogError("你沒有選任何物件QAQ");
        }
    }
}
