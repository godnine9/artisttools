Dynamic Ratio 動態比率：柔軟度，0-1之間，骨骼當前方向到目標方向的平滑度
Stiffness Force 剛度力：整體硬度，越小像絲帶，另有像鋼筋
Stiffness Curve 剛度曲線：硬度的逐點變化
Drag Force 阻力：力量衰減，先前越絲滑，越小像多節棍
Drag Curve 阻力曲線：力量逐點變化