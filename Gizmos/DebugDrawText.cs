﻿using UnityEngine;
using System.Collections;
#if UNITY_EDITOR
using UnityEditor;
#endif

public static class DebugDrawText
{
    public static void DrawText(Vector3 position, string text, Color textColor, Color backgroudColor)
    {
#if UNITY_EDITOR
        Color backupColor = GUI.color;
        Color backupBGColor = GUI.backgroundColor;

        GUI.color = Color.white;
        GUI.backgroundColor = Color.white;

        Handles.BeginGUI();
        {
            // get projection
            var view = UnityEditor.SceneView.currentDrawingSceneView;
            if (view != null)
            {
                Vector3 screenPos = view.camera.WorldToScreenPoint(position);
                Vector2 size = GUI.skin.label.CalcSize(new GUIContent(text));

                // Check depth
                if (screenPos.z > 0)
                {
                    // draw background
                    GUI.backgroundColor = backgroudColor;
                    GUI.TextField(new Rect(screenPos.x - (size.x / 2), -screenPos.y + view.position.height - (size.y * 1.5f), size.x, size.y), string.Empty);

                    // draw text
                    GUI.color = textColor;
                    GUI.Label(new Rect(screenPos.x - (size.x / 2), -screenPos.y + view.position.height - (size.y * 1.5f), size.x, size.y), text);
                }
            }
        }
        Handles.EndGUI();

        GUI.color = backupColor;
        GUI.backgroundColor = backupBGColor;
#endif
    }
}
